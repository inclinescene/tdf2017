//tdf17.frag by cxw/incline.  CC-BY-SA 3.0
//ncl01: Road Trip.  Released at Tokyo Demo Fest 2017.
//This file is generated from tdf17.frag.in using perlpp.  DO NOT EDIT.
//Use https://github.com/cxw42/perlpp/, branch 'defines'.



// Reminder: OpenGL-style coord system: +X right, +Y up, +Z toward viewer
// CONFIG CONSTANTS STRUCTS GLOBALS ///////////////////
// {{{1
precision highp int;    //play it safe.  Also covers
precision highp float;  //vectors and matrices.


// Adapt Shadertoy code to run in GLSL Sandbox //

// Data
uniform float time;
uniform vec2 resolution;
float iGlobalTime = time;
vec2 iResolution = resolution;

// Code
void mainImage( out vec4 fragColor, in vec2 fragCoord );

void main( void )
{
    vec4 color = vec4(0.0,0.0,0.0,1.0);
    mainImage( color, gl_FragCoord.xy );
    color.w = 1.0;
    gl_FragColor = color;
}


#define LOADING_FREQ (0.2)

#define S_SINE_FREQ (0.02380952380952380952380952380952)
    // 1/42.  sine letter frequency in cycles per X unit.
#define S_SINE_GROUP_FREQ (0.03125)
    // 1/32.  sine group frequency.  If > S_SINE_FREQ, sine gradually
    // shifts right on screen.
#define CYL_RADIUS (15.0)
    // radius of the cylinder for CYL and later parts
#define TUNNEL_ACCEL (17.0)
    // Acceleration during TUNNEL, in x units per sec^2
#define CUBE_FADEIN_TIME (3.0)
    //fadein in seconds

#define TWOSIDED_RATE (0.3)
    // how fast you go back and forth

#define PI (3.1415926535897932384626433832795028841971)
    // from memory :)
#define PI_OVER_2 (1.5707963267948966192313216916398)
#define PI_OVER_4 (0.78539816339744830961566084581988)
#define THREE_PI_OVER_4 (2.3561944901923449288469825374596)
#define TWO_PI (6.283185307179586)
#define ONE_OVER_TWO_PI (0.15915494309644431437107064141535)
    // not from memory :) :)

#define TEE_ZERO (0.001)
#define TEE_ONE (99999999999999999999.0)
    // +Inf => 1.0/0.0 gives "divide by zero during constant folding" error

#define LTR_Z_THICKNESS (1.0)
    // in voxels
#define EPS (0.000001)
    // Arbitrary choice

#define MAX_VOXEL_STEPS (60)
    // Empirical - gives a nice disappearing effect at the edges on my
    // test system
#define MAX_DIST (100.0)

// xport config
#define XP_GAIN_HZ (3.0)

#define XP_SHINE_LOW (6.0)
#define XP_SHINE_HIGH (32.0)
#define XP_SHINE_AIM (24.0)
    // what it settles down to
#define XP_SHINE_HZ (5.5)

// Sinusoids across the u axis (horz)

#define XP_H0_PER_U (30.00000000000000000000)
    // cycles per screen width (u coordinate 0..1)
#define XP_H0_PHASE (0.00000000000000000000)
    // initial phase
#define XP_H0_PHASE_PER_SEC (1.36590984938686665906)
    // how much the phase of the sinusoid changes per second

#define XP_H1_PER_U (10.00000000000000000000)
    // cycles per screen width (u coordinate 0..1)
#define XP_H1_PHASE (1.00000000000000000000)
    // initial phase
#define XP_H1_PHASE_PER_SEC (0.10000000000000000555)
    // how much the phase of the sinusoid changes per second

#define XP_H2_PER_U (5.00000000000000000000)
    // cycles per screen width (u coordinate 0..1)
#define XP_H2_PHASE (0.00000000000000000000)
    // initial phase
#define XP_H2_PHASE_PER_SEC (1.00000000000000000000)
    // how much the phase of the sinusoid changes per second


// }}}1

// CHARSET FOR WORLD-COORDINATE LETTERS ///////////////
// {{{1
/*
9    00000
8  1   2   3
7  1   2   3
6  1   2   3
5    44444
4  5       6
3  5       6
2 858      6
1 88877777
0 888
   0 12345 6
(lowercase x, dot, and bang are handled separately)
*/

// Character storage
#define NSEGS (9)
vec4 SEG_SHAPES[NSEGS];
    // All polys will be quads in the X-Y plane, Z=0.
    // All quad edges are parallel to the X or Y axis.
    // These quads are encoded in a vec4: (.x,.y) is the LL corner and
    // (.z,.w) is the UR corner (coords (x,y)).

vec4 SEG_VOXELS[NSEGS];
    // Same deal, but voxel offsets, start->last+1

// Grid parameters - 2D
#define GRID_CHARHT (10.0)
#define GRID_CHARWD (6.0)
    // Size of each character
#define GRID_PITCH (7.0)
    //each char takes up this much space.  Margin is added on the right
    //and is GRID_PITCH-GRID_CHARWD wide.
#define GRID_PITCH_RECIP (0.14285714285714285714285714285714)
    // avoid a division
#define GRID_VPITCH (12.0)
    // margin is added on top
#define THICKNESS (1.0)
    // how thick each stroke is

#define GRID_XSHIFT (GRID_PITCH * 0.5)
    // + pitch/0.5 because letters were snapping into
    // existence at the right side of the screen.

// Grid parameters - voxels.  Twice the size.
#define VGRID_CHARHT (GRID_CHARHT*2.0)
#define VGRID_CHARWD (GRID_CHARWD*2.0)
#define VGRID_PITCH (GRID_PITCH*2.0)
#define VGRID_PITCH_RECIP (GRID_PITCH_RECIP*0.5)
#define VGRID_VPITCH (GRID_VPITCH*2.0)

// For upright chars, each char (X,Y) goes from (PITCH*ofs, 0)->(.+WD,HT).

void init_charset()
{
    float halft = THICKNESS*0.5;
    float halfht = GRID_CHARHT * 0.5;

    SEG_SHAPES[0] = vec4(THICKNESS, GRID_CHARHT - THICKNESS, GRID_CHARWD-THICKNESS, GRID_CHARHT);
    SEG_SHAPES[1] = vec4(0.0,                   halfht, THICKNESS,             GRID_CHARHT - halft);
    SEG_SHAPES[2] = vec4(GRID_CHARWD*0.5-halft, halfht, GRID_CHARWD*0.5+halft, GRID_CHARHT - halft);
    SEG_SHAPES[3] = vec4(GRID_CHARWD-THICKNESS, halfht, GRID_CHARWD,           GRID_CHARHT - halft);
    SEG_SHAPES[4] = vec4(THICKNESS, halfht - halft, GRID_CHARWD-THICKNESS, halfht + halft);
    SEG_SHAPES[5] = vec4(0.0,                   halft,    THICKNESS,             halfht );
    SEG_SHAPES[6] = vec4(GRID_CHARWD-THICKNESS, halft,    GRID_CHARWD,           halfht );
    SEG_SHAPES[7] = vec4(THICKNESS, 0, GRID_CHARWD-THICKNESS, THICKNESS);
    SEG_SHAPES[8] = vec4(0.0, 0.0, THICKNESS, THICKNESS); //dot

    // Voxel grid #1 - not currently in use
    //Grid_Origin = vec3(GRID_XSHIFT, 0, 0);
    //Grid_Spacings = vec3(1.0);
    //Grid_Spacings_Inv = vec3(1.0)/Grid_Spacings;

    // TODO rewrite in terms of #defines.
    // Z, W are +1 so can use IsPointInRectXY, which does not include the
    // UR corner in the poly.
    // Size has been doubled, so we can use multiples of 0.5.
    SEG_VOXELS[0] = vec4(1.0, 9.0, 5.0,10.0)*vec4(2.0);
    SEG_VOXELS[1] = vec4(0.0, 5.5, 1.0, 9.5)*vec4(2.0);
    SEG_VOXELS[2] = vec4(3.0, 6.0, 4.0, 9.0)*vec4(2.0);
    SEG_VOXELS[3] = vec4(5.0, 5.5, 6.0, 9.5)*vec4(2.0);
    SEG_VOXELS[4] = vec4(1.0, 5.0, 5.0, 6.0)*vec4(2.0);
    SEG_VOXELS[5] = vec4(0.0, 1.5, 1.0, 5.5)*vec4(2.0);
    SEG_VOXELS[6] = vec4(5.0, 1.5, 6.0, 5.5)*vec4(2.0);
    SEG_VOXELS[7] = vec4(1.0, 1.0, 6.0, 2.0)*vec4(2.0);
    SEG_VOXELS[8] = vec4(0.0, 0.0, 2.0, 2.0)*vec4(2.0);

} //init_charset

// }}}1

// MESSAGE ////////////////////////////////////////////
// No music sync found
// Parts and start times
#define LOADING (0.0)
#define LOADING_START (0.00000000000000000000)
#define NOP (1.0)
#define NOP_START (30.00000000000000000000)
#define HEY (2.0)
#define HEY_START (31.00000000000000000000)
#define FALKEN (3.0)
#define FALKEN_START (32.00000000000000000000)
#define NOP2 (4.0)
#define NOP2_START (33.50000000000000000000)
#define LINE1 (5.0)
#define LINE1_START (34.25000000000000000000)
#define XPORT (6.0)
#define XPORT_START (41.00000000000000000000)
#define LINE2 (7.0)
#define LINE2_START (49.00000000000000000000)
#define LINE3 (8.0)
#define LINE3_START (58.75000000000000000000)
#define HOWTO (9.0)
#define HOWTO_START (85.75000000000000000000)
#define ENDPART (10.0)
#define ENDPART_START (98.00000000000000000000)

vec4 get_story(in float time)
{   //returns vec4(partnum, charidx_frac, first_charidx, clip_charidx)
    // NOTE: charidx_frac restarts at 0 each part!
    // first_charidx and clip_charidx are with respect to the whole messge.
    // Character indices starting with clip_charidx should not be displayed.
    float partnum, charidx_frac, first_charidx, clip_charidx;
    if(time<30.00000000000000000000) {
        partnum=LOADING;
        charidx_frac=(time-LOADING_START)*0.36666666666666664076;
        first_charidx=0.0;
        clip_charidx=10.0;
    } else

    if(time<31.00000000000000000000) {
        partnum=NOP;
        charidx_frac=(time-NOP_START)*1.00000000000000000000;
        first_charidx=11.0;
        clip_charidx=11.0;
    } else

    if(time<32.00000000000000000000) {
        partnum=HEY;
        charidx_frac=(time-HEY_START)*8.00000000000000000000;
        first_charidx=12.0;
        clip_charidx=19.0;
    } else

    if(time<33.50000000000000000000) {
        partnum=FALKEN;
        charidx_frac=(time-FALKEN_START)*8.00000000000000000000;
        first_charidx=20.0;
        clip_charidx=31.0;
    } else

    if(time<34.25000000000000000000) {
        partnum=NOP2;
        charidx_frac=(time-NOP2_START)*1.33333333333333325932;
        first_charidx=32.0;
        clip_charidx=32.0;
    } else

    if(time<41.00000000000000000000) {
        partnum=LINE1;
        charidx_frac=(time-LINE1_START)*4.00000000000000000000;
        first_charidx=33.0;
        clip_charidx=56.0;
    } else

    if(time<49.00000000000000000000) {
        partnum=XPORT;
        charidx_frac=(time-XPORT_START)*1.50000000000000000000;
        first_charidx=60.0;
        clip_charidx=71.0;
    } else

    if(time<58.75000000000000000000) {
        partnum=LINE2;
        charidx_frac=(time-LINE2_START)*4.00000000000000000000;
        first_charidx=72.0;
        clip_charidx=107.0;
    } else

    if(time<85.75000000000000000000) {
        partnum=LINE3;
        charidx_frac=(time-LINE3_START)*4.00000000000000000000;
        first_charidx=111.0;
        clip_charidx=215.0;
    } else

    if(time<98.00000000000000000000) {
        partnum=HOWTO;
        charidx_frac=(time-HOWTO_START)*4.00000000000000000000;
        first_charidx=219.0;
        clip_charidx=264.0;
    } else

    if(time<100.00000000000000000000) {
        partnum=ENDPART;
        charidx_frac=(time-ENDPART_START)*0.50000000000000000000;
        first_charidx=268.0;
        clip_charidx=268.0;
    } else

    {
        partnum=0.0;
        charidx_frac=0.0;
        first_charidx=0.0;
        clip_charidx=0.0;
    }

    return vec4(partnum,charidx_frac,first_charidx,clip_charidx);
} //get_story

vec4 get_seg_vec4(float vecidx) {
    if(vecidx>=34.0){
        if(vecidx>=51.0){
            if(vecidx>=59.0){
                if(vecidx>=63.0){
                    if(vecidx>=65.0){
                        if(vecidx>=67.0) return vec4(0.0,0.0,0.0,0.0);
                        if(vecidx>=66.0) return vec4(0.0,0.0,0.0,0.0);
                        if(vecidx>=65.0) return vec4(235.0,235.0,107.0,258.0);
                    }else{
                        if(vecidx>=64.0) return vec4(11.0,219.0,0.0,211.0);
                        if(vecidx>=63.0) return vec4(19.0,27.0,15.0,21.0);
                    }
                }else{
                    if(vecidx>=62.0) return vec4(107.0,179.0,178.0,0.0);
                    if(vecidx>=61.0) return vec4(178.0,16.0,248.0,256.0);
                    if(vecidx>=60.0) return vec4(162.0,123.0,107.0,179.0);
                    if(vecidx>=59.0) return vec4(178.0,235.0,256.0,59.0);
                }
            }else{
                if(vecidx>=55.0){
                    if(vecidx>=58.0) return vec4(0.0,122.0,235.0,30.0);
                    if(vecidx>=57.0) return vec4(0.0,27.0,26.0,178.0);
                    if(vecidx>=56.0) return vec4(114.0,187.0,19.0,118.0);
                    if(vecidx>=55.0) return vec4(0.0,0.0,0.0,163.0);
                }else{
                    if(vecidx>=54.0) return vec4(0.0,0.0,0.0,0.0);
                    if(vecidx>=53.0) return vec4(234.0,258.0,258.0,0.0);
                    if(vecidx>=52.0) return vec4(248.0,0.0,218.0,235.0);
                    if(vecidx>=51.0) return vec4(211.0,0.0,123.0,11.0);
                }
            }
        }else{
            if(vecidx>=42.0){
                if(vecidx>=46.0){
                    if(vecidx>=48.0){
                        if(vecidx>=50.0) return vec4(51.0,178.0,0.0,59.0);
                        if(vecidx>=49.0) return vec4(0.0,248.0,3.0,21.0);
                        if(vecidx>=48.0) return vec4(19.0,26.0,219.0,15.0);
                    }else{
                        if(vecidx>=47.0) return vec4(26.0,19.0,19.0,0.0);
                        if(vecidx>=46.0) return vec4(211.0,0.0,19.0,15.0);
                    }
                }else{
                    if(vecidx>=45.0) return vec4(27.0,0.0,242.0,35.0);
                    if(vecidx>=44.0) return vec4(30.0,123.0,0.0,178.0);
                    if(vecidx>=43.0) return vec4(107.0,242.0,123.0,107.0);
                    if(vecidx>=42.0) return vec4(16.0,0.0,118.0,235.0);
                }
            }else{
                if(vecidx>=38.0){
                    if(vecidx>=41.0) return vec4(26.0,178.0,187.0,0.0);
                    if(vecidx>=40.0) return vec4(0.0,15.0,2.0,11.0);
                    if(vecidx>=39.0) return vec4(18.0,123.0,211.0,178.0);
                    if(vecidx>=38.0) return vec4(187.0,3.0,218.0,0.0);
                }else{
                    if(vecidx>=37.0) return vec4(114.0,187.0,0.0,26.0);
                    if(vecidx>=36.0) return vec4(123.0,178.0,0.0,178.0);
                    if(vecidx>=35.0) return vec4(123.0,248.0,187.0,0.0);
                    if(vecidx>=34.0) return vec4(0.0,16.0,0.0,15.0);
                }
            }
        }
    }else{
        if(vecidx>=17.0){
            if(vecidx>=25.0){
                if(vecidx>=29.0){
                    if(vecidx>=31.0){
                        if(vecidx>=33.0) return vec4(0.0,19.0,124.0,30.0);
                        if(vecidx>=32.0) return vec4(11.0,187.0,0.0,16.0);
                        if(vecidx>=31.0) return vec4(11.0,19.0,18.0,21.0);
                    }else{
                        if(vecidx>=30.0) return vec4(0.0,16.0,0.0,21.0);
                        if(vecidx>=29.0) return vec4(163.0,162.0,235.0,72.0);
                    }
                }else{
                    if(vecidx>=28.0) return vec4(0.0,0.0,0.0,107.0);
                    if(vecidx>=27.0) return vec4(0.0,0.0,0.0,0.0);
                    if(vecidx>=26.0) return vec4(235.0,107.0,258.0,0.0);
                    if(vecidx>=25.0) return vec4(59.0,187.0,35.0,211.0);
                }
            }else{
                if(vecidx>=21.0){
                    if(vecidx>=24.0) return vec4(0.0,21.0,11.0,0.0);
                    if(vecidx>=23.0) return vec4(178.0,187.0,11.0,248.0);
                    if(vecidx>=22.0) return vec4(11.0,0.0,123.0,178.0);
                    if(vecidx>=21.0) return vec4(21.0,0.0,19.0,123.0);
                }else{
                    if(vecidx>=20.0) return vec4(0.0,211.0,27.0,0.0);
                    if(vecidx>=19.0) return vec4(0.0,0.0,0.0,0.0);
                    if(vecidx>=18.0) return vec4(0.0,0.0,0.0,0.0);
                    if(vecidx>=17.0) return vec4(178.0,179.0,35.0,0.0);
                }
            }
        }else{
            if(vecidx>=8.0){
                if(vecidx>=12.0){
                    if(vecidx>=14.0){
                        if(vecidx>=16.0) return vec4(211.0,59.0,235.0,35.0);
                        if(vecidx>=15.0) return vec4(178.0,35.0,123.0,107.0);
                        if(vecidx>=14.0) return vec4(0.0,0.0,0.0,0.0);
                    }else{
                        if(vecidx>=13.0) return vec4(178.0,114.0,123.0,178.0);
                        if(vecidx>=12.0) return vec4(21.0,178.0,114.0,0.0);
                    }
                }else{
                    if(vecidx>=11.0) return vec4(26.0,59.0,0.0,30.0);
                    if(vecidx>=10.0) return vec4(35.0,35.0,218.0,0.0);
                    if(vecidx>=9.0) return vec4(0.0,0.0,122.0,234.0);
                    if(vecidx>=8.0) return vec4(0.0,0.0,0.0,0.0);
                }
            }else{
                if(vecidx>=4.0){
                    if(vecidx>=7.0) return vec4(179.0,107.0,258.0,0.0);
                    if(vecidx>=6.0) return vec4(51.0,123.0,162.0,118.0);
                    if(vecidx>=5.0) return vec4(0.0,0.0,0.0,0.0);
                    if(vecidx>=4.0) return vec4(122.0,179.0,218.0,0.0);
                }else{
                    if(vecidx>=3.0) return vec4(0.0,0.0,0.0,0.0);
                    if(vecidx>=2.0) return vec4(256.0,256.0,0.0,0.0);
                    if(vecidx>=1.0) return vec4(21.0,11.0,219.0,256.0);
                    if(vecidx>=0.0) return vec4(162.0,235.0,123.0,248.0);
                }
            }
        }
    }
    return vec4(0.0);
} //get_seg_vec4

#define NUM_CHARS_IN_MESSAGE (269.0)
float get_seg_mask(float charidx)
{
    if(charidx>=NUM_CHARS_IN_MESSAGE) return 0.0; //blank at the end
    float vecidx = charidx * 0.250000000;
    float subidx = mod(charidx, 4.0);
    vec4 v = get_seg_vec4(vecidx);
    float rv = v[0];
    rv = mix(rv, v[1], step(1.0, subidx));
    rv = mix(rv, v[2], step(2.0, subidx));
    rv = mix(rv, v[3], step(3.0, subidx));
    return rv;
} //get_seg_mask

// Camera and light prototypes

void do_cl_loading(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_nop(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_hey(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_falken(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_nop2(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_line1(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_xport(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_line2(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_line3(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_howto(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_endpart(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);

void do_camera_light(in float partnum, in float charidx_frac,
                        out vec3 camera_pos,
                        out vec3 camera_look_at, out vec3 camera_up,
                        out float fovy_deg, out vec3 light_pos)
{   // Camera and light dispatcher
    if(partnum>=LINE1) {

        if(partnum==LINE1) {
            do_cl_line1(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==XPORT) {
            do_cl_xport(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==LINE2) {
            do_cl_line2(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==LINE3) {
            do_cl_line3(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==HOWTO) {
            do_cl_howto(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==ENDPART) {
            do_cl_endpart(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        {
            camera_pos=vec3(0.0,0.0,10.0);    //default
            camera_look_at=vec3(0.0);
            camera_up=vec3(0.0, 1.0, 0.0);
            fovy_deg=45.0;
            light_pos=camera_pos;
        }
    } else {

        if(partnum==LOADING) {
            do_cl_loading(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==NOP) {
            do_cl_nop(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==HEY) {
            do_cl_hey(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==FALKEN) {
            do_cl_falken(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==NOP2) {
            do_cl_nop2(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        {
            camera_pos=vec3(0.0,0.0,10.0);    //default
            camera_look_at=vec3(0.0);
            camera_up=vec3(0.0, 1.0, 0.0);
            fovy_deg=45.0;
            light_pos=camera_pos;
        }
    }
} //do_camera_light

#define HEY_REALSTART (16.00000000000000000000)
#define FALKEN_REALSTART (24.00000000000000000000)
#define XPORT_NCHARS (11.00000000000000000000)
#define LOADING_NCHARS (10.00000000000000000000)
#define XPORT_FADEIN_DURATION (5.0)


// UTIL ///////////////////////////////////////////////
// {{{1
mat4 my_transpose(in mat4 inMatrix)

{
    // Modified from
    // http://stackoverflow.com/a/18038495/2877364 by
    // http://stackoverflow.com/users/2507370/jeb
    vec4 i0 = inMatrix[0];
    vec4 i1 = inMatrix[1];
    vec4 i2 = inMatrix[2];
    vec4 i3 = inMatrix[3];

    vec4 o0 = vec4(i0.x, i1.x, i2.x, i3.x);
    vec4 o1 = vec4(i0.y, i1.y, i2.y, i3.y);
    vec4 o2 = vec4(i0.z, i1.z, i2.z, i3.z);
    vec4 o3 = vec4(i0.w, i1.w, i2.w, i3.w);

    mat4 outMatrix = mat4(o0, o1, o2, o3);

    return outMatrix;
}

void lookat(in vec3 in_eye, in vec3 in_ctr, in vec3 in_up,
            out mat4 view, out mat4 view_inv)
{
    // From Mesa glu.  Thanks to
    // http://learnopengl.com/#!Getting-started/Camera
    // and https://www.opengl.org/wiki/GluLookAt_code

    vec3 forward, side, up;

    forward=normalize(in_ctr-in_eye);
    up = in_up;
    side = normalize(cross(forward,up));
    up = cross(side,forward);   // already normalized since both inputs are
        //now side, up, and forward are orthonormal

    mat4 orient, where;

    // Note: in Mesa gluLookAt, a C matrix is used, so the indices
    // have to be swapped compared to that code.
    vec4 x4, y4, z4, w4;
    x4 = vec4(side,0);
    y4 = vec4(up,0);
    z4 = vec4(-forward,0);
    w4 = vec4(0,0,0,1);
    orient = my_transpose(mat4(x4, y4, z4, w4));

    where = mat4(1.0); //identity (1.0 diagonal matrix)
    where[3] = vec4(-in_eye, 1);

    view = (orient * where);

    // Compute the inverse for later
    view_inv = mat4(x4, y4, z4, -where[3]);
    view_inv[3][3] = 1.0;   // since -where[3].w == -1, not what we want
        // Per https://en.wikibooks.org/wiki/GLSL_Programming/Vertex_Transformations ,
        // M_{view->world}
} //lookat

void gluPerspective(in float fovy_deg, in float aspect,
                    in float near, in float far,
                    out mat4 proj, out mat4 proj_inv)
{   // from mesa glu-9.0.0/src/libutil/project.c.
    // Thanks to https://unspecified.wordpress.com/2012/06/21/calculating-the-gluperspective-matrix-and-other-opengl-matrix-maths/

    float fovy_rad = radians(fovy_deg);
    float dz = far-near;
    float sin_fovy = sin(fovy_rad);
    float cot_fovy = cos(fovy_rad) / sin_fovy;

    proj=mat4(0);
    //[col][row]
    proj[0][0] = cot_fovy / aspect;
    proj[1][1] = cot_fovy;

    proj[2][2] = -(far+near)/dz;
    proj[2][3] = -1.0;

    proj[3][2] = -2.0*near*far/dz;

    // Compute the inverse matrix.
    // http://bookofhook.com/mousepick.pdf
    float a = proj[0][0];
    float b = proj[1][1];
    float c = proj[2][2];
    float d = proj[3][2];
    float e = proj[2][3];

    proj_inv = mat4(0);
    proj_inv[0][0] = 1.0/a;
    proj_inv[1][1] = 1.0/b;
    proj_inv[3][2] = 1.0/e;
    proj_inv[2][3] = 1.0/d;
    proj_inv[3][3] = -c/(d*e);
} //gluPerspective

void compute_viewport(in float x, in float y, in float w, in float h,
                        out mat4 viewp, out mat4 viewp_inv)
{
    // See https://en.wikibooks.org/wiki/GLSL_Programming/Vertex_Transformations#Viewport_Transformation
    // Also mesa src/mesa/main/viewport.c:_mesa_get_viewport_xform()

    viewp = mat4(0);
    // Reminder: indexing is [col][row]
    viewp[0][0] = w/2.0;
    viewp[3][0] = x+w/2.0;

    viewp[1][1] = h/2.0;
    viewp[3][1] = y+h/2.0;

    // assumes n=0 and f=1,
    // which are the default for glDepthRange.
    viewp[2][2] = 0.5;  // actually 0.5 * (f-n);
    viewp[3][2] = 0.5;  // actually 0.5 * (n+f);

    viewp[3][3] = 1.0;

    //Invert.  Done by hand.
    viewp_inv = mat4(1.0);
    viewp_inv[0][0] = 2.0/w;    // x->x
    viewp_inv[3][0] = -1.0 - (2.0*x/w);

    viewp_inv[1][1] = 2.0/h;    // y->y
    viewp_inv[3][1] = -1.0 - (2.0*y/h);

    viewp_inv[2][2] = 2.0;      // z->z
    viewp_inv[3][2] = -1.0;

}  //compute_viewport

// https://www.opengl.org/wiki/Compute_eye_space_from_window_space

vec4 wts(in mat4 modelviewproj, in mat4 viewport,
                in vec3 pos)
{   // world to screen coordinates
    vec4 clipvertex = modelviewproj * vec4(pos,1.0);
    vec4 ndc = clipvertex/clipvertex.w;
    vec4 transformed = viewport * ndc;
    return transformed;
} //wts

// screen to world: http://bookofhook.com/mousepick.pdf
vec4 WorldRayFromScreenPoint(in vec2 scr_pt,
    in mat4 view_inv,
    in mat4 proj_inv,
    in mat4 viewp_inv)
{   // Returns world coords of a point on a ray passing through
    // the camera position and scr_pt.

    vec4 ndc = viewp_inv * vec4(scr_pt,0.0,1.0);
        // z=0.0 => it's a ray.  0 is an arbitrary choice in the
        // view volume.
        // w=1.0 => we don't need to undo the perspective divide.
        //      So clip coords == NDC

    vec4 view_coords = proj_inv * ndc;
        // At this point, z=0 will have become something in the
        // middle of the projection volume, somewhere between
        // near and far.
    view_coords = view_coords / view_coords.w;
        // Keepin' it real?  Not sure what happens if you skip this.
    //view_coords.w = 0.0;
        // Remove translation components.  Note that we
        // don't use this trick.
    vec4 world_ray_point = view_inv * view_coords;
        // Now scr_pt is on the ray through camera_pos and world_ray_point
    return world_ray_point;
} //WorldRayFromScreenPoint

vec3 hsv2rgb(vec3 c) {
    // by hughsk, from https://github.com/hughsk/glsl-hsv2rgb/blob/master/index.glsl .
    // All inputs range from 0 to 1.
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

float scalesin(in float bot, in float top, in float x)
{   //rescale [-1,1] to [bot, top]
    return mix(bot, top, clamp((x+1.0)*0.5, 0.0, 1.0));
}



// }}}1

// VOXEL MARCHING
// {{{1

// Variables for voxel marching
struct VM2State {
    // Parameters
    vec3 origin;
    vec3 direction;
    vec3 world_min;
    vec3 world_max;

    // Internals
    vec3 curr;      //where we are now - was x, y, z vars
    vec3 stepdir;   //was step[XYZ]
    vec3 tMax;
    vec3 tDelta;
    float max_t;
}; //VM2State

// By http://gamedev.stackexchange.com/users/8806/maxim-kamalov , aka
// dogfuntom, https://gist.github.com/dogfuntom .
// See http://gamedev.stackexchange.com/questions/47362/cast-ray-to-select-block-in-voxel-game#comment188335_49423
// Modified from https://gist.github.com/cc881c8fc86ad43d55d8.git
//// Heavily based on:
//// http://gamedev.stackexchange.com/a/49423/8806

float intbound(float s, float ds)
{
    // Some kind of edge case, see:
    // http://gamedev.stackexchange.com/questions/47362/cast-ray-to-select-block-in-voxel-game#comment160436_49423 :
        // "The edge case is where a coordinate of the ray origin is an
        //integer value, and the corresponding part of the ray direction is
        //negative. The initial tMax value for that axis should be zero, since
        //the origin is already at the bottom edge of its cell, but it is
        //instead 1/ds causing one of the other axes to be incremented instead.
        //The fix is to write intfloor to check if both ds is negative and s is
        //an integer value (mod returns 0), and return 0.0 in that case. –
        //codewarrior Dec 24 '14 at 12:00"

    // by http://gamedev.stackexchange.com/users/57468/codewarrior
    bool sIsInteger = (fract(s)==0.0);  //TODO check against epsilon?
    if (ds < 0.0 && sIsInteger)
        return 0.0;

    return (
        ( (ds > 0.0) ? (ceil(s) - s) : (s - floor(s)) ) / abs(ds)
    );
} //intbound

bool VM2_init(out VM2State state,
    in vec3 origin, in vec3 direction, in float max_dist,
    in vec3 world_min, in vec3 world_max)
{ //The initialization portion of VM2_raycast (q.v.).
  //Returns true if successful.

    if(length(direction)==0.0) {
        return false;   // *** EXIT POINT ***
    }

    state.origin = origin;
    state.direction = direction;
    state.world_min = world_min;     // TODO? make sure they are ints?
    state.world_max = world_max;

    state.curr = floor(origin);
    state.stepdir = sign(direction);

    state.tMax.x = intbound(origin.x, direction.x);
    state.tMax.y = intbound(origin.y, direction.y);
    state.tMax.z = intbound(origin.z, direction.z);

    state.tDelta.x = state.stepdir.x / direction.x;
    state.tDelta.y = state.stepdir.y / direction.y;
    state.tDelta.z = state.stepdir.z / direction.z;

    state.max_t = max_dist / length(direction);
    return true;
} //VM2_init

//DEBUG: these are floats.  For production, change them back to int.
#define VM2_HIT (1.0)
#define VM2_NOTYET (-1.0)
#define VM2_DONE (0.0)

vec4 VM2_step(inout VM2State state,
                out vec3 voxel, out vec3 hitpoint, out vec3 normal)
{ //returns:
  // VM2_HIT    if we hit a voxel in the world;
  // VM2_NOTYET if we have not yet reached the world; or
  // VM2_DONE   if we have traced off the end of the world or have gone
  //            too far along the ray.
  // If VM2_HIT, voxel and normal are filled in:
  //    voxel       coordinates of the voxel we're in
  //    hitpoint    The actual point where the ray hit the voxel
  //    normal      normal of the voxel at hitpoint

    vec3 ret_normal;    //value to be returned
    float hit_t;        //where we actually hit

    // Go to the next voxel.
    //DEBUG: The *0.05's below are to rescale for visibility, and are debug.
    if (state.tMax.x < state.tMax.y) {
        if (state.tMax.x < state.tMax.z) {
            if (state.tMax.x > state.max_t) return vec4(state.tMax*0.05,VM2_DONE);
            state.curr.x += state.stepdir.x;    // Update which cube we are now in.
            hit_t = state.tMax.x;               // Record where we hit the cube
            state.tMax.x += state.tDelta.x;
                // Adjust state.tMax.x to the next X-oriented crossing
            ret_normal = vec3(-state.stepdir.x, 0.0, 0.0);
                // Record the normal vector of the voxel we just entered.
        } else {
            if (state.tMax.z > state.max_t) return vec4(state.tMax*0.05,VM2_DONE);
            state.curr.z += state.stepdir.z;
            hit_t = state.tMax.z;
            state.tMax.z += state.tDelta.z;
            ret_normal = vec3(0.0, 0.0, -state.stepdir.z);
        }
    } else {
        if (state.tMax.y < state.tMax.z) {
            if (state.tMax.y > state.max_t) return vec4(state.tMax*0.05,VM2_DONE);
            state.curr.y += state.stepdir.y;
            hit_t = state.tMax.y;
            state.tMax.y += state.tDelta.y;
            ret_normal = vec3(0.0, -state.stepdir.y, 0.0);
        } else {
            if (state.tMax.z > state.max_t) return vec4(state.tMax*0.05,VM2_DONE);
            state.curr.z += state.stepdir.z;
            hit_t = state.tMax.z;
            state.tMax.z += state.tDelta.z;
            ret_normal = vec3(0.0, 0.0, -state.stepdir.z);
        }
    } //end conditionals

    // Check if we're past the world
    if( (state.stepdir.x>0.0) ?
        (state.curr.x>=state.world_max.x) : (state.curr.x<state.world_min.x) )
        return vec4(1.0,0.0,0.0,VM2_DONE);
    if( (state.stepdir.y>0.0) ?
        (state.curr.y>=state.world_max.y) : (state.curr.y<state.world_min.y) )
        return vec4(0.0,1.0,0.0,VM2_DONE);
    if( (state.stepdir.z>0.0) ?
        (state.curr.z>=state.world_max.z) : (state.curr.z<state.world_min.z) )
        return vec4(0.0,0.0,1.0,VM2_DONE);

    // Check if we're not yet at the world.
    // TODO in VM2_init, fast-forward to the boundary of the world so that
    // this case never happens.
    if( (state.stepdir.x>0.0) ?
        (state.curr.x<state.world_min.x) : (state.curr.x>=state.world_max.x) )
        return vec4(0.5,0.0,0.0,VM2_NOTYET);
    if( (state.stepdir.y>0.0) ?
        (state.curr.y<state.world_min.y) : (state.curr.y>=state.world_max.y) )
        return vec4(0.0,0.5,0.0,VM2_NOTYET);
    if( (state.stepdir.z>0.0) ?
        (state.curr.z<state.world_min.z) : (state.curr.z>=state.world_max.z) )
        return vec4(0.0,0.0,0.5,VM2_NOTYET);

    // If we made it here, we are in a voxel cell.
    voxel = state.curr;
    hitpoint = state.origin + hit_t*state.direction;
    normal = ret_normal;
    return vec4(voxel,VM2_HIT);     //voxel is debug
} //VM2_step

// }}}1

// GEOMETRY HIT-TESTING ///////////////////////////////
// {{{1



// Faster routine for the special case of the main text
vec3 HitZZero(vec3 camera_pos, vec3 rayend)
{   // Find where the ray meets the z=0 plane.  The ray is
    // camera_pos + t*(rayend - camera_pos) per Hook.
    float hit_t = -camera_pos.z / (rayend.z - camera_pos.z);
    return (camera_pos + hit_t * (rayend-camera_pos));
} //HitZZero

// --- IsPointInRectXY ---
// All polys will be quads in the X-Y plane, Z=0.
// All quad edges are parallel to the X or Y axis.
// These quads are encoded in a vec4: (.x,.y) is the LL corner and
// (.z,.w) is the UR corner (coords (x,y)).  The UR corner is not
// inclued in the poly.

bool IsPointInRectXY(in vec4 poly_coords, in vec2 world_xy_of_point)
{
    // return true if world_xy_of_point is within the poly defined by
    // poly_coords in the Z=0 plane.
    // I.e., xy >= poly_coords.xy, and xy < poly_coords.zw.
    // I can test in 2D rather than 3D because all the geometry
    // has z=0 and all the quads are planar.

    float x_test, y_test;
    x_test = step(poly_coords.x, world_xy_of_point.x) *
            (1.0 - step(poly_coords.z, world_xy_of_point.x));
        // step() is 1.0 if world.x >= poly_coords.x
        // 1-step() is 1.0 if world.x < poly_coords.z
    y_test = step(poly_coords.y, world_xy_of_point.y) *
            (1.0 - step(poly_coords.w, world_xy_of_point.y));

    return ( (x_test>=0.9) && (y_test >= 0.9) );
        // Not ==1.0 because these are floats!

} //IsPointInRectXY



// }}}1

// TEXT RENDERING /////////////////////////////////////
// {{{1

// Text-rendering internal parameters
#define LETTER_EPSILON (0.001)
    // small enough for our purposes.
#define SIDE_LETTERS (4)
    // How many letters to render on each side of the current one.
    // Set to fill the screen at the desired aspect ratio and orientation.

bool is_in_zzero_message(in vec2 world_xy_of_point,
                         in float middle_charidx, in float middle_x,
                         in float clip_charidx)
{   // returns true iff world_xy_of_point is in a letter
    // upright in the z=0 plane.
    // Letters are extracted from the message, with message[middle_charidx]
    // being displayed with its LL corner at (middle_x, 0, 0).
    // Characters starting from clip_charidx are not hit.

    // Check each letter in turn
    for(int ltr_idx=0; ltr_idx<(2*SIDE_LETTERS+1); ++ltr_idx) {
        float letter_delta = float(ltr_idx-SIDE_LETTERS-1);
        float thisletterindex = letter_delta + middle_charidx;
            // so the middle element of ltr_idx maps to middle_charidx

        if(thisletterindex >= clip_charidx) {
            break;  // no more letters
        }

        float mask = get_seg_mask(thisletterindex);
            // the segments for this letter

        // Early exit on spaces
        if(mask <= LETTER_EPSILON) {
            continue; //to next letter
        }

        // Where is this letter on the X axis?
        float ofs = (letter_delta*GRID_PITCH) + middle_x;

        // check each segment in turn
        for(int seg_idx=0; seg_idx<NSEGS; ++seg_idx) {
            if(mod(mask, 2.0)>LETTER_EPSILON) {
                // Where is this segment of this letter?
                vec4 theshape = SEG_SHAPES[seg_idx];
                theshape += vec4(ofs, 0.0, ofs, 0.0);
                    //shift it over to the right place

                // Check if we are in the segment
                if(IsPointInRectXY(theshape, world_xy_of_point)) {
                    return true;    // as soon as we're in a segment, we don't need to check any others
                }

            } //endif this segment is in mask

            mask = floor(mask * 0.5);
                //move to next bit and drop fractional part

            // Early exit when you run out of segments
            if(mask<=LETTER_EPSILON) {
                break; //to next letter
            }
        } //foreach segment

    } //foreach letter

    return false;
} //is_in_zzero_message

bool is_in_basic_message(in vec2 pt,
    in float first_charidx, in float clip_charidx)
{   // returns true iff world_xy_of_point is the message for this part,
    // which begins with first_charidx at x=0, upright in the z=0 plane.

    float nchars = (clip_charidx-first_charidx);
        //not ()+1 because clip_charidx is one past the last char to show.
    if( (pt.x<0.0) || (pt.x>=nchars*GRID_PITCH) ) {
        return false;   //outside - can't hit
    }

    if( (pt.y<0.0) || (pt.y>GRID_CHARHT*GRID_VPITCH) ) {
        return false;   //ditto
    }

    // Which letter are we in?  There can be only one.
    float ltridx = floor(pt.x/GRID_PITCH);
    float ofs = ltridx * GRID_PITCH;
    float mask = get_seg_mask(first_charidx + ltridx);

    // Early exit on spaces
    if(mask <= LETTER_EPSILON) {
        return false;
    }

    // check each segment in turn
    for(int seg_idx=0; seg_idx<NSEGS; ++seg_idx) {
        if(mod(mask, 2.0)>LETTER_EPSILON) {
            // Where is this segment of this letter?
            vec4 theshape = SEG_SHAPES[seg_idx];
            theshape += vec4(ofs, 0.0, ofs, 0.0);
                //shift it over to the right place

            // Check if we are in the segment
            if(IsPointInRectXY(theshape, pt)) {
                return true;    // as soon as we're in a segment,
            }                   // we don't need to check any others

        } //endif this segment is in mask

        mask = floor(mask * 0.5);
            //move to next bit and drop fractional part

        // Early exit when you run out of segments
        if(mask<=LETTER_EPSILON) {
            return false;       // no more chances
        }
    } //foreach segment

    return false;
} //is_in_basic_message

// }}}1

// LETTER RENDERING ///////////////////////////////////
// {{{1

// Text-rendering internal parameters
#define LETTER_EPSILON (0.001)
    // small enough for our purposes.

/*vec3*/ bool voxel_is_in_message(
    in vec3 origin, in vec3 direction,
    in float charidx_frac, in float first_charidx, in float clip_charidx,
    out vec3 voxel, out vec3 hitpoint, out vec3 normal)
{   //Determine whether _hitlocn_ is in the upright message at the current
    //point in the story, as determined by charidx_frac.
    //Returns true on hit.  If true, _voxel_ holds the grid coordinates of the
    //voxel that was hit, _hitpoint_ holds the actual point hit, and
    //_normal_ is the normal of the voxel face that was hit.

    //Retval storage, so we don't trash the out parameters if there's no hit.
    vec3 ret_voxel, ret_hitpoint, ret_normal;

    // Setup voxel marching
    bool ok;
    VM2State state;
    ok = VM2_init(state, origin, direction, MAX_DIST,
        vec3(0.0,0.0,-LTR_Z_THICKNESS),  //world_min
        vec3((clip_charidx-first_charidx)*VGRID_PITCH,   //world_max - last+1
            VGRID_CHARHT,
            1.0));  // 1.0 => voxels (:,:,0) are the farthest voxels forward.
                    // (+Z is towards viewer normally)

    if(!ok) return false;   // *** EXIT POINT *** can't init => can't hit

    // Run the marching loop.  At each voxel, check the mask for only
    // the letter that voxel might be in.
    for(int step_idx=0; step_idx<MAX_VOXEL_STEPS; ++step_idx) {
        vec4 /*int*/ hit = VM2_step(state, ret_voxel, ret_hitpoint, ret_normal);

        if(hit.w == VM2_DONE) return false;   // *** EXIT POINT ***

        if(hit.w == VM2_NOTYET) continue;     // to the next voxel step

        // If we got here, we are in a voxel that is in the world.

        // Determine which character cell this voxel is in.
        // The text starts at x=0 for each part.
        float voxel_rel_charidx = floor(ret_voxel.x * VGRID_PITCH_RECIP);
            //which grid cell we're in
        float voxel_abs_charidx = voxel_rel_charidx + first_charidx;
            // Where we are in the story.
            // Don't need to check clip_charidx since that is rolled into
            // the world_max limits in VM2_init().
        float mask = get_seg_mask(voxel_abs_charidx);
            // the segments for this letter
        //return vec3(mask/255.0); //DEBUG

        // Early exit on spaces
        if(mask <= LETTER_EPSILON) {
            continue; //to next voxel step
        }

        // Where is this letter on the X axis?
        float letter_xorigin = voxel_rel_charidx * VGRID_PITCH;
        float voxel_x_within_cell = ret_voxel.x - letter_xorigin;
            // since voxel cells are every 1 unit at present

        //return vec3(voxel_x_within_cell/20.0, voxel.y/10.0, voxel.z/10.0);  //DEBUG

        // check each segment in turn
        for(int seg_idx=0; seg_idx<NSEGS; ++seg_idx) {
            if(mod(mask, 2.0)>LETTER_EPSILON) {     //this segment is lit
                // Are we in this segment?
                if(IsPointInRectXY(SEG_VOXELS[seg_idx],
                                    vec2(voxel_x_within_cell, ret_voxel.y))) {
                   // A hit!  A very palpable hit.
                   voxel = ret_voxel;
                   hitpoint = ret_hitpoint;
                   normal = ret_normal;
                   return /*vec3(1.0)*/ true;
                }
            } //endif this segment is in mask

            mask = floor(mask * 0.5);
                //move to next bit and drop any fractional part

            // Early exit when you run out of segments
            if(mask<=LETTER_EPSILON) {
                break;  //done with this letter - go to next voxel step
            }
        } //foreach segment

    } //for each voxel step

    return false;   //else return no-hit
} //voxel_is_in_message

// }}}1

// CAMERA AND LIGHT ///////////////////////////////////
// {{{1

// --- Helpers ---

#define GAMMA (2.2)
#define ONE_OVER_GAMMA (0.45454545454545454545454545454545)

vec3 phong_color(
    in vec3 pixel_pos, in vec3 normal, in vec3 camera_pos,      // Scene
    in vec3 light_pos, in vec3 ambient_matl,                    // Lights
    in vec3 diffuse_matl, in vec3 specular_matl,                // Lights
    in float shininess)                                         // Material
{   // Compute pixel color using Blinn-Phong shading with a white light.
    // Modified from
    // https://en.wikipedia.org/wiki/Blinn%E2%80%93Phong_shading_model
    // Normal must be normalized on input.  All inputs are world coords.
    // Set shininess <=0 to turn off specular highlights.
    // Objects are one-sided.

    vec3 light_dir = normalize(light_pos - pixel_pos);
    vec3 eye_dir = normalize(camera_pos - pixel_pos);

    if(dot(light_dir, eye_dir) < 0.0) {
        return ambient_matl;       // Camera behind the object
    }

    float lambertian = max(0.0, dot(light_dir, normal));        // Diffuse

    float specular = 0.0;
    if((lambertian > 0.0) && (shininess > 0.0)) {               // Specular
        vec3 reflectDir = reflect(-light_dir, normal);
        float specAngle = max(dot(reflectDir, eye_dir), 0.0);
        specular = pow(specAngle, shininess);
    }
    lambertian = pow(lambertian, ONE_OVER_GAMMA);
    specular = pow(specular, ONE_OVER_GAMMA);

    vec3 retval = ambient_matl + lambertian*diffuse_matl +
        specular*specular_matl;

    return clamp(retval, 0.0, 1.0);     // no out-of-range values, please!

} //phong_color

highp vec3 pos_clelies(in float the_time, in float radius)
{   //Clelies curve
    //thanks to http://wiki.roblox.com/index.php?title=Parametric_equations
    vec3 pos; float m = 0.8;
    highp float smt = sin(m*the_time);
    pos.x = radius * smt*cos(the_time);
    pos.y = radius * smt*sin(the_time);
    pos.z = radius * cos(m*the_time);
    return pos;
} //camerapos

// --- Per-part routines ---

void do_cl_nop(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{
    camera_pos = vec3(0.0,0.0,10.0);    //default
    camera_look_at = vec3(0.0);
    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = 45.0;
    light_pos = camera_pos;
} //do_cl_nop

void do_cl_loading(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_nop(pn, cf, cp, cla, cu, fd, lp); }
void do_cl_hey(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_nop(pn, cf, cp, cla, cu, fd, lp); }
void do_cl_falken(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_nop(pn, cf, cp, cla, cu, fd, lp); }
void do_cl_nop2(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_nop(pn, cf, cp, cla, cu, fd, lp); }

//s_plain = Scroller, Plain
void do_cl_s_plain(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{
    camera_pos = vec3(charidx_frac*GRID_PITCH-5.0, GRID_CHARHT*0.5, 10.0);
    camera_look_at = vec3(camera_pos.x+3.0, GRID_CHARHT*0.5,0);
    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = 45.0;
    light_pos = camera_pos;
    light_pos.y += 4.0 * sin(charidx_frac);
} //do_cl_s_plain

void do_cl_line1(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_s_plain(pn, cf, cp, cla, cu, fd, lp); }

void do_cl_xport(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{ //static camera
    camera_pos = vec3(/*charidx_frac*/
        floor(XPORT_NCHARS/2.0)*GRID_PITCH+GRID_PITCH*0.3, GRID_CHARHT*0.5 + 0.5, 10.0);
    camera_look_at = vec3(camera_pos.x, GRID_CHARHT*0.5,0);
    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = 68.0;
    light_pos = camera_pos;
} //do_cl_xport

void do_cl_line2(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_s_plain(pn, cf, cp, cla, cu, fd, lp); }

void do_cl_line3(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{ //For the voxel part.
    camera_pos = vec3(charidx_frac*VGRID_PITCH-5.0, VGRID_CHARHT*0.5+0.5, 20.0);
    camera_look_at = vec3(camera_pos.x-0.5, VGRID_CHARHT*0.5,0.0);

    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = 45.0;
    light_pos = vec3(
        camera_pos.x + VGRID_PITCH*sin(TWO_PI*0.125*charidx_frac),
        camera_pos.y+6.0,
        camera_pos.z-2.0
    );
} //do_cl_line3

void do_cl_howto(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_line3(pn, cf, cp, cla, cu, fd, lp); }

void do_cl_endpart(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_nop(pn, cf, cp, cla, cu, fd, lp); }

// }}}1

// ARTISTRY ///////////////////////////////////////////
// {{{1
void do_hey(in float partnum, in vec2 charpos, in float clip_charidx,
            out vec3 diffuse_matl)
{
    vec2 pos;
    diffuse_matl = vec3(0.0);

    //"Hey"
    pos = charpos - vec2(0.0, 1.0*GRID_VPITCH); // move up one line
    if(is_in_zzero_message(pos, HEY_REALSTART+4.0, GRID_PITCH*5.0, clip_charidx)) {
        diffuse_matl = vec3(1.0, 0.0, 0.0);
        return;
    }

    pos = charpos; // - vec2(0.0, 0.0*GRID_VPITCH);
    if( (partnum==FALKEN) &&
        is_in_zzero_message(pos, FALKEN_REALSTART+4.0, GRID_PITCH*5.0, clip_charidx)) {
        diffuse_matl = vec3(1.0, 1.0, 1.0);
        return;
    }
} //do_hey

void color_xport(in vec2 wpt, in vec2 uvpt, in float dt,
                out vec3 ambient_matl, out vec3 diffuse_matl,
                out float shininess)
{ // World point, Fragment point ([0,1] range), time within the effect.
  // Fades in over time=[0, XPORT_FADEIN_DURATION].

    float ramp = smoothstep(0.0, XPORT_FADEIN_DURATION, dt);
    ambient_matl = vec3(0.0);   //for now

    // Shininess
    float s_dt = scalesin(-PI_OVER_2, PI_OVER_2, dt/XPORT_FADEIN_DURATION);
        // s_dt goes from 0 to pi/2
    float s_top = mix(XP_SHINE_HIGH, XP_SHINE_AIM, dt/XPORT_FADEIN_DURATION);
    float s_bot = mix(XP_SHINE_LOW, XP_SHINE_AIM, dt/XPORT_FADEIN_DURATION);
    float shine = sin(TWO_PI*XP_SHINE_HZ*dt);
    shininess = scalesin(s_bot, s_top, shine); // [XP_SHINE_LOW, X~_HIGH]

    // Overall gain
    float g_top = clamp(ramp*1.5, 0.0, 1.0);
    float g_bot = ramp*ramp;    // <=ramp, so <= g_top
    float gain = sin(TWO_PI*XP_GAIN_HZ*dt);
    gain = scalesin(g_bot, g_top, gain);    // [0,1]

    // Localized gain - horizontal
    float s;
    
        s = sin(TWO_PI*XP_H0_PER_U * uvpt.x +
            TWO_PI*XP_H0_PHASE +
            TWO_PI*XP_H0_PHASE_PER_SEC * dt);
        s = mix(s, 1.0, ramp);     // gain effects are 1.0 at the end
        gain *= scalesin(0.0, 1.0, s);
    
        s = sin(TWO_PI*XP_H1_PER_U * uvpt.x +
            TWO_PI*XP_H1_PHASE +
            TWO_PI*XP_H1_PHASE_PER_SEC * dt);
        s = mix(s, 1.0, ramp);     // gain effects are 1.0 at the end
        gain *= scalesin(0.0, 1.0, s);
    
        s = sin(TWO_PI*XP_H2_PER_U * uvpt.x +
            TWO_PI*XP_H2_PHASE +
            TWO_PI*XP_H2_PHASE_PER_SEC * dt);
        s = mix(s, 1.0, ramp);     // gain effects are 1.0 at the end
        gain *= scalesin(0.0, 1.0, s);
    

    diffuse_matl = gain * vec3(0.2, 0.2, 1.0);
} //color_xport

// }}}1

// MAIN ///////////////////////////////////////////////
// {{{1

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    // Init
    float the_time = iGlobalTime; //0.5*mod(iGlobalTime, 16.0)+S_PLAIN_START;
    //float the_time = iGlobalTime + XPORT_START - 2.0;
    //float the_time = mod(iGlobalTime,LINE2_START-XPORT_START)+XPORT_START; //DEBUG
        //0.05*mod(3.48, 20.0)+S_PLAIN_START; //DEBUG test case
    vec2 uv = fragCoord.xy/iResolution.xy;
    init_charset();

#if 0
    // Grayscale ramp for reference
    if(fragCoord.y<10.0) {
        fragColor = vec4(vec3(fragCoord.x/iResolution.x),1.0);
        return; //EXIT POINT
    }
#endif

    // --- Story ---
    vec4 story = get_story(the_time);
    float partnum=story[0], charidx_frac=story[1];
    float first_charidx=story[2], clip_charidx=story[3];

    // --- Camera and light ---
    vec3 camera_pos, camera_look_at, camera_up, light_pos;
    float fovy_deg;

    do_camera_light(partnum, charidx_frac,
        camera_pos, camera_look_at, camera_up, fovy_deg, light_pos);

    // Camera processing
    mat4 view, view_inv;
    lookat(camera_pos, camera_look_at, camera_up,
            view, view_inv);

    mat4 proj, proj_inv;
    gluPerspective(fovy_deg, iResolution.x/iResolution.y, 1.0, 10.0,
                    proj, proj_inv);

    mat4 viewport, viewport_inv;
    compute_viewport(0.0, 0.0, iResolution.x, iResolution.y,
                        viewport, viewport_inv);

    // --- Geometry ---

    vec3 rayend = WorldRayFromScreenPoint(fragCoord,
                                    view_inv, proj_inv, viewport_inv).xyz;
    vec3 ray_direction = rayend - camera_pos;

    // Each part determines world coords of the hit, normal at the
    // hit point, and base color of the geometry.

    vec3 wc_pixel;  // world coords of this pixel
    vec3 wc_normal; // ditto for the normal
    vec3 ambient_matl = vec3(0.1);
    vec3 diffuse_matl = vec3(0.0);
        // material - light is always white.  Alpha is always 1.
    float shininess = 4.0;  //Phong shininess
    bool did_hit = false;   //if not did_hit, just use _diffuse_.

    if(partnum == LOADING) {
        // Are we in the message?
        
            // Grid of character cells

        vec3 hitlocn = HitZZero(camera_pos, rayend);
            // hitlocn is where it hits z=0, where the letters are

        // Size and move the letters
        hitlocn.xy *= vec2( 5.00000000000000000000, 3.00000000000000000000);
        hitlocn.xy += vec2( -15.00000000000000000000, 24.00000000000000000000);
        did_hit = is_in_basic_message(hitlocn.xy, first_charidx, clip_charidx);

        if(did_hit) {
            float dt = mod(the_time * 0.2, 1.0);
            diffuse_matl = hsv2rgb(vec3(
                mod(the_time, 1.0),
                1.0,
                smoothstep(0.0, 0.5, dt) * (1.0-smoothstep(0.5, 1.0, dt))
            ));
            did_hit = false;    // only use diffuse_matl
        } else {    // not in the text
            // GLSL Sandbox default, tweaked slightly
            float color = 0.0;
            float rotation = mod(LOADING_FREQ*the_time, TWO_PI);
            mat2 rot = mat2(cos(rotation), sin(rotation), -sin(rotation), cos(rotation));
            vec2 position = rot * uv;
            color += sin( position.x * cos( the_time / 15.0 ) * 80.0 ) + cos( position.y * cos( the_time / 15.0 ) * 10.0 );
            color += sin( position.y * sin( the_time / 10.0 ) * 40.0 ) + cos( position.x * sin( the_time / 25.0 ) * 40.0 );
            color += sin( position.x * sin( the_time / 5.0 ) * 10.0 ) + sin( position.y * sin( the_time / 35.0 ) * 80.0 );
            color *= sin( the_time / 10.0 ) * 0.5;

            diffuse_matl = vec3( color, color * 0.5, sin( color + the_time / 3.0 ) * 0.75 );
        } //endif in text else

        diffuse_matl *= (1.0-smoothstep(NOP_START-2.0, NOP_START, the_time));
            // fade out gently

    } else if( (partnum == NOP) || (partnum == ENDPART) ) {    // black screens
        diffuse_matl = vec3(0.0);

    } else if( (partnum == HEY) || (partnum == FALKEN) ) {  // Static text
        // Straight 2d
        // Grid for this part is 9 chars across and 3 high,
        // offset by half a character vertically.
        vec2 charpos = uv * vec2(9.0,3.0);
            //now charpos is 0..8 horz and 0..3 vert
        charpos.y -= 0.5;    // now -0.5..2.5 vert are on screen
        charpos *= vec2(GRID_PITCH, GRID_VPITCH);
            // Now in coordinates of the segments

        do_hey(partnum, charpos, clip_charidx, diffuse_matl);
            //did_hit stays false so we just use diffuse_matl.

    } else if(partnum == XPORT) {                       // Beam me up
        diffuse_matl = vec3(0.0);
        vec3 hitlocn = HitZZero(camera_pos, rayend);
            // hitlocn is where it hits z=0, where the letters are
        did_hit = is_in_basic_message(hitlocn.xy, first_charidx, clip_charidx);
        if(did_hit) {
            color_xport(hitlocn.xy, uv, the_time - XPORT_START,
                ambient_matl, diffuse_matl, shininess);
            did_hit = false;    // only use diffuse_matl
            //diffuse_matl = vec3(0.0,1.0,0.0); did_hit = false; //DEBUG
        }
    } else if(partnum <= LINE2) {      // Basic scrollers, 2d

        vec3 hitlocn = HitZZero(camera_pos, rayend);
            // hitlocn is where it hits z=0, where the letters are
        did_hit = is_in_basic_message(hitlocn.xy, first_charidx, clip_charidx);
        if(did_hit) {
            wc_pixel = hitlocn;
            wc_normal = vec3(0.0,0.0,-1.0 + 2.0*step(0.0, camera_pos.z));
                // normal Z is -1 if camera_pos.z<0.0, and +1 otherwise.
                // This benefits TWOSIDED.
            ambient_matl = vec3(0.2, 0.2, 0.1);
            diffuse_matl = vec3(0.6,0.6,0.3);
            shininess = 50.0;
        }  // else diffuse is the default (0,0,0).

    } else {        // Basic scrollers, voxel
        diffuse_matl = vec3(0.0);
        vec3 voxel;
        did_hit =
            voxel_is_in_message(camera_pos, ray_direction,
                                  charidx_frac, first_charidx, clip_charidx,
                                  voxel, wc_pixel, wc_normal);
        if(did_hit) {
            diffuse_matl = vec3(0.2, 0.3, 0.9);
            shininess = 20.0;
        }
    } //endif part switch

    // --- Lighting ---
    // Phong shading based on the Geometry section's output values

    if(did_hit) {               // a hit
        vec3 rgb = phong_color(
            wc_pixel, wc_normal, camera_pos, light_pos,
            ambient_matl, diffuse_matl, vec3(1.0), shininess);

        fragColor = vec4(rgb, 1.0);
    } else {                    // no hit - just use diffuse_matl
        fragColor = vec4(diffuse_matl, 1.0);
    }

} //mainImage

// }}}1

// vi: set ts=4 sts=4 sw=4 et ai foldmethod=marker foldenable foldlevel=0: //

