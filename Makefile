# Makefile for bday by cxw/Incline

TARGETS=tdf17.frag
TARGET_GS=tdf17gs.frag
	# for GLSL Sandbox
DEPS=gen-gfx.py
PPFLAGS=
	# set to -Dgs for glsl sandbox

export SHELL=/bin/bash

all: $(TARGETS)
	for f in $(TARGETS) ; do \
		cat "$$f" | putclip ; \
		echo "Paste $$f where it goes then hit Enter" ; \
		read ; \
	done

gs: $(TARGET_GS)

$(TARGET_GS): tdf17.frag.in $(DEPS)
	perlpp -Dgs -o $@ $<
	cat "$@" | putclip

# Dependency order.  TODO: be more sophisticated about this.
$(TARGETS): $(DEPS)
#$(SYNC): gen-msx.py
#gen-gfx.py: $(SYNC)

# Run the music first.
#bday-gfx.frag: bday-msx.frag

# Rules
# preprocess files with perlpp from https://github.com/cxw42/perlpp,
# which is forked from https://github.com/d-ash/perlpp.
# This rule modified from http://stackoverflow.com/a/397232/2877364 by
# http://stackoverflow.com/users/49301/nathan-kurz .
% :: %.in
	perlpp $(PPFLAGS) -o $@ $<
# NOTE: the gen-msx.py and gen-gfx.py calls are done by perlpp

# vi: set ts=8 sts=0 sw=8 noet ai ff=unix: #

